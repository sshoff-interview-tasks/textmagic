FROM php:8.2-fpm

ARG WORKDIR=/app

RUN groupadd -r developer -g 1000 && \
    useradd -u 1000 -r -g developer -s /bin/sh -c "Docker image user" developer

USER root

WORKDIR /tmp

RUN apt-get update && apt-get install -y \
        apt-transport-https \
        p7zip-full \
        libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql \
    && apt-get clean && apt-get autoclean \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \ 
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer

USER developer

WORKDIR $WORKDIR

COPY . $WORKDIR
