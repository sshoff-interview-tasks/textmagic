<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230928110107 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE answer (id UUID NOT NULL, question_id UUID NOT NULL, value VARCHAR(255) NOT NULL, correct BOOLEAN NOT NULL, active BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DADD4A251E27F6BF ON answer (question_id)');
        $this->addSql('COMMENT ON COLUMN answer.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN answer.question_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN answer.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN answer.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE question (id UUID NOT NULL, question VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN question.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN question.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN question.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A251E27F6BF FOREIGN KEY (question_id) REFERENCES question (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql('
            INSERT INTO "question" VALUES 
                ( \'ce44c33f-bf4b-42d6-85a0-9486eea265a0\', \'1 + 1 =\', true, NOW(), NOW() ),
                ( \'fa36cb7b-f849-4394-89dd-20a67e9fd1ad\', \'2 + 2 =\', true, NOW(), NOW() ),
                ( \'ae769753-742f-421e-8afa-aee4ab8a1650\', \'3 + 3 =\', true, NOW(), NOW() ),
                ( \'784103f8-e411-43b1-af95-aef6628cd866\', \'4 + 4 =\', true, NOW(), NOW() ),
                ( \'daff5fa5-84af-4476-b6b3-1b59817474ca\', \'5 + 5 =\', true, NOW(), NOW() ),
                ( \'dd36cf03-c2c9-42aa-9d52-ce6a64d46964\', \'6 + 6 =\', true, NOW(), NOW() ),
                ( \'b2ccdad6-0b45-457b-95c3-cb6d406c3e0a\', \'7 + 7 =\', true, NOW(), NOW() ),
                ( \'add32937-c919-4bc4-bacf-d2148d4018ed\', \'8 + 8 =\', true, NOW(), NOW() ),
                ( \'376f5cda-64f0-4429-84c9-367032159467\', \'9 + 9 =\', true, NOW(), NOW() ),
                ( \'2854a26d-cf75-42d3-b574-b2afb83fd86e\', \'10 + 10 =\', true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'6abc05fe-e2e8-4cc7-a802-64472247b038\', \'ce44c33f-bf4b-42d6-85a0-9486eea265a0\', \'3\', false, true, NOW(), NOW() ),
                ( \'88ca0df3-6240-440e-bc14-f938ee72be13\', \'ce44c33f-bf4b-42d6-85a0-9486eea265a0\', \'2\', true, true, NOW(), NOW() ),
                ( \'7b9763a1-5548-48f9-9dba-062db3b7716c\', \'ce44c33f-bf4b-42d6-85a0-9486eea265a0\', \'0\', false, true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'dc7b4034-6e2e-4d8b-bb48-8c2bb8339377\', \'fa36cb7b-f849-4394-89dd-20a67e9fd1ad\', \'4\', true, true, NOW(), NOW() ),
                ( \'6163d094-0939-4296-b3fb-f88a84c38291\', \'fa36cb7b-f849-4394-89dd-20a67e9fd1ad\', \'3 + 1\', true, true, NOW(), NOW() ),
                ( \'6041a1f7-4df5-45de-9484-9a560a252395\', \'fa36cb7b-f849-4394-89dd-20a67e9fd1ad\', \'10\', false, true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'c7a2f576-382b-46a6-9b2c-e67ccf71fe9b\', \'ae769753-742f-421e-8afa-aee4ab8a1650\', \'1 + 5\', true, true, NOW(), NOW() ),
                ( \'d4b6c874-3a10-4f02-a755-14902f06494f\', \'ae769753-742f-421e-8afa-aee4ab8a1650\', \'1\', false, true, NOW(), NOW() ),
                ( \'bbef8483-4e79-44bb-950e-bfd39ee0b408\', \'ae769753-742f-421e-8afa-aee4ab8a1650\', \'6\', true, true, NOW(), NOW() ),
                ( \'be60906e-1c99-4de1-98ac-0901c86b340d\', \'ae769753-742f-421e-8afa-aee4ab8a1650\', \'2 + 4\', true, true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'04fc0928-76a3-4ea7-8656-6cd81caf7d64\', \'784103f8-e411-43b1-af95-aef6628cd866\', \'8\', true, true, NOW(), NOW() ),
                ( \'53c61135-6237-4f9a-812e-0fedf1ddd922\', \'784103f8-e411-43b1-af95-aef6628cd866\', \'4\', false, true, NOW(), NOW() ),
                ( \'e0636853-3ed7-4d61-86c8-7993c0a6c421\', \'784103f8-e411-43b1-af95-aef6628cd866\', \'0\', false, true, NOW(), NOW() ),
                ( \'e5557642-d822-4de3-92b8-df9004ad4fd6\', \'784103f8-e411-43b1-af95-aef6628cd866\', \'0 + 8\', true, true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'f22c6c36-f9ba-48c0-acf0-0ca61d21ca78\', \'daff5fa5-84af-4476-b6b3-1b59817474ca\', \'6\', false, true, NOW(), NOW() ),
                ( \'0b224719-a65a-478b-8bc2-4b0e55c2f41e\', \'daff5fa5-84af-4476-b6b3-1b59817474ca\', \'18\', false, true, NOW(), NOW() ),
                ( \'027777b1-21ac-4676-a07a-feed143854c2\', \'daff5fa5-84af-4476-b6b3-1b59817474ca\', \'10\', true, true, NOW(), NOW() ),
                ( \'e1cefee4-6261-4a74-baf2-b25d1414084d\', \'daff5fa5-84af-4476-b6b3-1b59817474ca\', \'9\', false, true, NOW(), NOW() ),
                ( \'55e4e438-8cfc-40fc-b737-ada70611b806\', \'daff5fa5-84af-4476-b6b3-1b59817474ca\', \'0\', false, true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'914e807b-dbab-46d5-99af-a2d5a7ed900b\', \'dd36cf03-c2c9-42aa-9d52-ce6a64d46964\', \'3\', false, true, NOW(), NOW() ),
                ( \'ce47a410-dc42-482f-8f5b-61d84789f7ab\', \'dd36cf03-c2c9-42aa-9d52-ce6a64d46964\', \'9\', false, true, NOW(), NOW() ),
                ( \'b5d85d5c-fa9f-4329-9938-8733cd3db533\', \'dd36cf03-c2c9-42aa-9d52-ce6a64d46964\', \'0\', false, true, NOW(), NOW() ),
                ( \'4dc2db32-15f8-46e1-a370-f6380b19741f\', \'dd36cf03-c2c9-42aa-9d52-ce6a64d46964\', \'12\', true, true, NOW(), NOW() ),
                ( \'4e89ffe7-746c-41a4-a97a-45b39a64b802\', \'dd36cf03-c2c9-42aa-9d52-ce6a64d46964\', \'5 + 7\', true, true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'f915f525-573c-476b-9bb6-f03d035a62c6\', \'b2ccdad6-0b45-457b-95c3-cb6d406c3e0a\', \'5\', false, true, NOW(), NOW() ),
                ( \'6a73770e-280b-4892-a0be-2d427eaa9efe\', \'b2ccdad6-0b45-457b-95c3-cb6d406c3e0a\', \'14\', true, true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'d39182bb-e80a-4699-b900-d3274bd390d2\', \'add32937-c919-4bc4-bacf-d2148d4018ed\', \'16\', true, true, NOW(), NOW() ),
                ( \'2831cecf-410a-4071-be05-dd2a4bab5cde\', \'add32937-c919-4bc4-bacf-d2148d4018ed\', \'12\', false, true, NOW(), NOW() ),
                ( \'66cd654f-86c9-4a57-a5d7-068e27903708\', \'add32937-c919-4bc4-bacf-d2148d4018ed\', \'9\', false, true, NOW(), NOW() ),
                ( \'c7d84b2d-d9df-4d23-8e20-5f19a1857923\', \'add32937-c919-4bc4-bacf-d2148d4018ed\', \'5\', false, true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'0415b92a-0a07-479a-ab9d-0584bdad9538\', \'376f5cda-64f0-4429-84c9-367032159467\', \'18\', true, true, NOW(), NOW() ),
                ( \'3948b855-1cf0-4085-b27c-b836a57b9d20\', \'376f5cda-64f0-4429-84c9-367032159467\', \'9\', false, true, NOW(), NOW() ),
                ( \'2b419e48-4de2-4593-9351-3aafcd7ddaa3\', \'376f5cda-64f0-4429-84c9-367032159467\', \'17 + 1\', true, true, NOW(), NOW() ),
                ( \'96356b29-4379-4c6f-83e6-e3d427afc128\', \'376f5cda-64f0-4429-84c9-367032159467\', \'2 + 16\', true, true, NOW(), NOW() );
        ');

        $this->addSql('
            INSERT INTO "answer" VALUES
                ( \'b6dbec4e-c873-49ec-a690-d152fadc077f\', \'2854a26d-cf75-42d3-b574-b2afb83fd86e\', \'0\', false, true, NOW(), NOW() ),
                ( \'7dc552a5-aeba-4776-854b-f9bc0a16fdfc\', \'2854a26d-cf75-42d3-b574-b2afb83fd86e\', \'2\', false, true, NOW(), NOW() ),
                ( \'7d13a3d1-0a41-48ca-9693-5bd14e4a5b88\', \'2854a26d-cf75-42d3-b574-b2afb83fd86e\', \'8\', false, true, NOW(), NOW() ),
                ( \'5db9dcab-914c-42f6-98c3-23a086e687bb\', \'2854a26d-cf75-42d3-b574-b2afb83fd86e\', \'20\', true, true, NOW(), NOW() );
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE answer DROP CONSTRAINT FK_DADD4A251E27F6BF');
        $this->addSql('DROP TABLE answer');
        $this->addSql('DROP TABLE question');
    }
}
