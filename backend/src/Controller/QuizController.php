<?php

namespace App\Controller;

use \Exception;
use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\Quiz;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class QuizController extends AbstractController
{
    #[
        Route('/quiz/create/', name: 'app_quiz_create', methods: ['GET']),
        Route('/quiz/create/{limit}', name: 'app_quiz_create_with_limit', methods: ['GET'])
    ]
    public function create(
        int $limit = null,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $quiz = [];
        $response = [];

        $questionRepo = $entityManager->getRepository(Question::class);
        $answerRepo = $entityManager->getRepository(Answer::class);
        $quizRepo   = $entityManager->getRepository(Quiz::class);

        $questions = $questionRepo->getShuffled($limit);

        foreach ($questions as $question) {
            $answers = $answerRepo->getShuffled(Uuid::fromString($question['id']));

            $answersForJson = [];
            $correctAnswersForJson = [];

            foreach ($answers as $answer) {
                $answersForJson[] = array(
                    'uuid' => $answer['id'],
                    'value' => $answer['value']
                );

                if ($answer['correct']) {
                    $correctAnswersForJson[] = $answer['id'];
                }
            }

            $response[] = array(
                'uuid' => $question['id'],
                'question' => $question['question'],
                'answers' => $answersForJson,
            );

            $quiz[] = array(
                'uuid' => $question['id'],
                'question' => $question['question'],
                'answers' => $answersForJson,
                'correct' => $correctAnswersForJson
            );
        }

        $quizEntity = $quizRepo->save($quiz);

        // TODO: Rewrite to DTO
        return $this->json(
            array(
                'uuid' => $quizEntity->getId(),
                'quiz' => $response
            )
        );
    }

    #[
        Route('/quiz/answer/', name: 'app_quiz_answer', methods: ['POST'])
    ]
    public function answer(
        Request $request,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $suggestion = json_decode($request->getContent(), true);

        // TODO: Rewrite to DTO
        $constraints = new Assert\Collection([
            'quiz' => [new Assert\NotBlank, new Assert\Uuid],
            'question' => [new Assert\NotBlank, new Assert\Uuid],
            'suggestions' => [new Assert\NotBlank, new Assert\Type('array')]
        ]);

        $violations = $validator->validate($suggestion, $constraints);

        if (count($violations) > 0) {
            $errorsString = $violations->__toString();

            // TODO: Create a wrapper for errors
            return new JsonResponse(
                array(
                    'error' => $errorsString
                ),
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        foreach ($suggestion['suggestions'] as $s) {
            try {
                Uuid::fromString($s);
            } catch (Exception $e) {
                return new JsonResponse(
                    array(
                        'error' => 'Array `suggestions` must contain UUIDs. `' . $s . '` is not valid UUID.'
                    ),
                    JsonResponse::HTTP_BAD_REQUEST
                );
            }
        }

        $quizRepo = $entityManager->getRepository(Quiz::class);

        try {
            $quizRepo->check($suggestion);
        } catch (Exception $e) {
            return new JsonResponse(
                array(
                    'error' => $e->getMessage()
                ),
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        return $this->json('OK');
    }

    #[
        Route('/quiz/result/{quiz}', name: 'app_quiz_result', methods: ['GET'])
    ]
    public function result(
        $quiz,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $constraints = new Assert\Collection([
            'quiz' => [new Assert\NotBlank, new Assert\Uuid]
        ]);

        $violations = $validator->validate(['quiz' => $quiz], $constraints);

        if (count($violations) > 0) {
            $errorsString = $violations->__toString();

            return new JsonResponse(
                array(
                    'error' => $errorsString
                ),
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        $quizRepo = $entityManager->getRepository(Quiz::class);

        try {
            $result = $quizRepo->result($quiz);
        } catch (Exception $e) {
            return new JsonResponse(
                array(
                    'error' => $e->getMessage()
                ),
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        return $this->json($result);
    }
}
