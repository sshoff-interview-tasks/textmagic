<?php

namespace App\Repository;

use App\Entity\Quiz;
use DateTimeImmutable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Uid\Uuid;

/**
 * @extends ServiceEntityRepository<Quiz>
 *
 * @method Quiz|null find($id, $lockMode = null, $lockVersion = null)
 * @method Quiz|null findOneBy(array $criteria, array $orderBy = null)
 * @method Quiz[]    findAll()
 * @method Quiz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuizRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Quiz::class);
    }

    public function save($quizeState, Uuid $id = null): Quiz
    {
        if ($id) {
            $quiz = $this->find(Uuid::fromString($id));
        } else {
            $quiz   = new Quiz();
        }

        $quiz->setQuiz($quizeState);

        $quiz->setUpdatedAt(new DateTimeImmutable());

        $this->getEntityManager()->persist($quiz);

        $this->getEntityManager()->flush();

        return $quiz;
    }

    public function check($suggestion)
    {
        $quizUuid = $suggestion['quiz'];
        $questionUuis = $suggestion['question'];
        $suggestions = $suggestion['suggestions'];

        $quizEntity = $this->find(Uuid::fromString($quizUuid));

        if (!$quizEntity) {
            throw new Exception('Quiz with UUID ' . $quizUuid . ' does not exist.');
        }

        $quiz = $quizEntity->getQuiz();

        $questionFound = false;

        foreach ($quiz as $key => $question) {
            if ($question['uuid'] == $questionUuis) {
                $questionFound = true;

                if (isset($question['suggestions'])) {
                    throw new Exception('You can not answer the same question twice.');
                } else {
                    $quiz[$key]['suggestions'] = $suggestions;
                }

                if (($errorSuggestion = $this->checkAnswers($question['answers'], $suggestions)) !== true) {
                    throw new Exception('Suggestion with UUID ' . $errorSuggestion . ' does not exist in this question.');
                }

                $quiz[$key]['result'] = $this->checkSuggestions($question['correct'], $suggestions);
            }
        }

        if (!$questionFound) {
            throw new Exception('Question with UUID ' . $suggestion['quiz'] . ' does not exist in this quiz.');
        }

        $quizEntity->setQuiz($quiz);

        $quizEntity->setUpdatedAt(new DateTimeImmutable());

        $this->getEntityManager()->persist($quizEntity);

        $this->getEntityManager()->flush();

        return $quizEntity;
    }

    public function result($uuid)
    {
        $quizEntity = $this->find(Uuid::fromString($uuid));

        if (!$quizEntity) {
            throw new Exception('Quiz with UUID ' . $uuid . ' does not exist.');
        }

        $quiz = $quizEntity->getQuiz();

        $correct = [];
        $incorrect = [];

        foreach ($quiz as $key => $question) {
            if (!isset($question['result'])) {
                throw new Exception('Question #' . ($key + 1) . ' (' . $question['uuid'] . ') does not solve.');
            }

            if ($question['result']) {
                $correct[] = '#' . ($key + 1) . ': ' . $question['question'];
            } else {
                $incorrect[] = '#' . ($key + 1) . ': ' . $question['question'];
            }
        }

        // TODO: Create an object
        return [
            'correct'=> $correct,
            'incorrect'=> $incorrect
        ];
    }

    private function checkAnswers($answers, $suggestions)
    {
        $variants = [];

        foreach ($answers as $answer) {
            $variants[] = $answer['uuid'];
        }

        foreach ($suggestions as $suggestion) {
            if (!in_array($suggestion, $variants)) {
                return $suggestion;
            }
        }

        return true;
    }

    private function checkSuggestions($correct, $suggestions): bool
    {
        foreach ($suggestions as $suggestion) {
            if (!in_array($suggestion, $correct)) {
                return false;
            }
        }

        return true;
    }

    //    /**
    //     * @return Quiz[] Returns an array of Quiz objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('q')
    //            ->andWhere('q.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('q.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Quiz
    //    {
    //        return $this->createQueryBuilder('q')
    //            ->andWhere('q.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
