Description of the task: [open](https://docs.google.com/document/d/1glUxOj4YYyRwGVhaIweiFkC3IR-7Q7iP9O5xaRKS2ho/edit#heading=h.d173un2go0hk)

## How to run

```bash
cd docker && docker-compose up -d
```

## How to use

### Create new quiz
```bash
curl --location 'http://127.0.0.1:8383/quiz/create/'
```

The response will be like this:
```json
{
    "uuid": "018ae121-0af3-746a-a5b7-b33a9bcdf89b",
    "quiz": [
        {
            "uuid": "ae769753-742f-421e-8afa-aee4ab8a1650",
            "question": "3 + 3 =",
            "answers": [
                {
                    "uuid": "c7a2f576-382b-46a6-9b2c-e67ccf71fe9b",
                    "value": "1 + 5"
                },
                {
                    "uuid": "d4b6c874-3a10-4f02-a755-14902f06494f",
                    "value": "1"
                },
                {
                    "uuid": "be60906e-1c99-4de1-98ac-0901c86b340d",
                    "value": "2 + 4"
                },
                {
                    "uuid": "bbef8483-4e79-44bb-950e-bfd39ee0b408",
                    "value": "6"
                }
            ]
        },
        {
            "uuid": "2854a26d-cf75-42d3-b574-b2afb83fd86e",
            "question": "10 + 10 =",
            "answers": [
                {
                    "uuid": "5db9dcab-914c-42f6-98c3-23a086e687bb",
                    "value": "20"
                },
                {
                    "uuid": "7d13a3d1-0a41-48ca-9693-5bd14e4a5b88",
                    "value": "8"
                },
                {
                    "uuid": "b6dbec4e-c873-49ec-a690-d152fadc077f",
                    "value": "0"
                },
                {
                    "uuid": "7dc552a5-aeba-4776-854b-f9bc0a16fdfc",
                    "value": "2"
                }
            ]
        }
    ]
}
```

### Make an answer

The field `quiz` is `uuid` from the response to the previous request (Create new quiz).

The field `question` is `quiz.uuid` from the response to the previous request.

The fields in `suggestions` are `quiz.answers.uuid` from the response to the previous request.

```bash
curl --location 'http://127.0.0.1:8383/quiz/answer/' \
--header 'Content-Type: application/json' \
--data '{
    "quiz": "018ae121-0af3-746a-a5b7-b33a9bcdf89b",
    "question": "2854a26d-cf75-42d3-b574-b2afb83fd86e",
    "suggestions": [
        "5db9dcab-914c-42f6-98c3-23a086e687bb",
        "7d13a3d1-0a41-48ca-9693-5bd14e4a5b88"
    ]
}'
```

### How to get the result

The last param in the URL is `uuid` from the response to the first request (Create new quiz).

```bash
curl --location 'http://127.0.0.1:8383/quiz/result/018ae121-0af3-746a-a5b7-b33a9bcdf89b/'
```

## How to stop

```bash
cd docker && docker-compose down -v
```